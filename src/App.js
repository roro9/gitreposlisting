import './App.css';
import React, { Component } from 'react';
import config from './config.js';
import Header from './Components/Header.js';
import SearchBar from './Components/SearchBar.js';
import Results from './Components/Results.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: 'Search Github',
      repositories: []
    }
  }

  onChangeSearchBox = (e) => {
    this.setState({ searchText: e.target.value});
  }

  onSearchSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation(); // for performance..no need for event to bubble up
    
    console.log('Search for: ' + this.state.searchText);

    // don't search if text box empty
    if(!this.state.searchText) {
      this.setState({repositories: []});
      return;
    }

    const url = `${config.api}/${config.searchReposTag}=${this.state.searchText}`
    fetch(url)
    .then(res => {
      if(!res.ok) {
        throw new Error(res.statusText);
      }
      return res.json();
    }).then(json => {
      if(json && json.items && json.items.slice(0,config.limit)) {
        this.setState({repositories: json.items.slice(0,config.limit)});
      } else {
        throw new Error('Invalid github api data');
      }
    }).catch(err => {
      console.error(err);
    });
  }

  render() {
    return (
      <div className="App">
        <Header/>
        <SearchBar onChange={this.onChangeSearchBox} onSubmit={this.onSearchSubmit} value={this.state.searchText}/>
        <Results repos={this.state.repositories}/>
      </div>
    );
  }
}

export default App;
