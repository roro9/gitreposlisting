import React, { Component } from 'react';


class SearchBar extends Component {
  render() {
    return (
      <div className="App-Search">
        <form onSubmit={this.props.onSubmit}>
          <input type="text" value={this.props.value} onChange={this.props.onChange}/>
          <input type="submit" value="Search"/>
      </form>
      </div>
    );
  }
}


export default SearchBar;
