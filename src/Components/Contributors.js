import React, { Component } from 'react';

class ContribCard extends Component {
  render() {
    let {avatar_url, login, html_url, contributions} = this.props.data;
    
    return (
      <div className="contrib-row">
        <div style={{ 
          backgroundImage: "url(" + avatar_url + ")", 
          width: "50px",
          height: "50px",
          margin: "0 0.75rem 0 0",
          border: "1px solid #b9b9b9"
        }}>
        </div>
        
        <div className="details">
          <div> <a href={html_url} target="_blank">{login}</a></div>
          <div>
            {contributions}
            <i className="fa fa-code"></i>
          </div>
        </div>
  
      </div>
    );
  }
}

class Contributors extends Component {
  render() {
    
    return (
        <div className="contributors">
          
          <div className="list">
            <div className="repo-name">
              Contributors for: {this.props.repoName} 
            </div>
            {
              this.props.data.map(contrib => {
                return <ContribCard key={contrib.id} data={contrib}/>
              })
            }
          </div>
          
          <div className="footer" onClick={this.props.handleFetchMore}>Fetch More</div>
        </div>
    );
  }
}

export default Contributors;
