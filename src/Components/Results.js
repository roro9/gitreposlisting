import React, { Component } from 'react';
import Repositories from './Repositories.js';
import Contributors from './Contributors.js';

class Results extends Component {

  constructor(props) {
    super(props);
    this.state = {
      contributors: [],
      contributorsLimit: 10,
      contributorsPanelVisible: false,
      contributorForRepoName: 'Repo Name',
      selectedRepo: {}
    }
  }
  
  /**
   * fetch data for repo and save state with contributors
   */
  fetchData = (repo) => {
    if(!repo.contributors_url) {
      return;
    }

    fetch(repo.contributors_url)
    .then(res => {
      if(!res.ok) {
        throw new Error(res.statusText);
      }
      return res.json();
    }).then(json => {
      if(json) {
        this.setState({
          contributors: json.slice(0, this.state.contributorsLimit),
          contributorForRepoName: repo.name,
          contributorsPanelVisible: true,
          selectedRepo: repo,
        }, () => {
          console.log(this.state);
        });

      } else {
        throw new Error('Invalid github api data');
      }
    }).catch(err => {
      console.error(err);
    });
  }

  onTopContributorsClick = (repo) => {
    console.log('load contributors for repo: ' + repo.name);
    
    // reset limit back to 10...in case last time user clicked fetch
    this.setState({
      contributorsLimit: 10
    }, ()=> {
      this.fetchData(repo);
    });
  }
  
  /**
   * increase contibutors limit by 5
   */
  handleFetchMore = () => {
    this.setState({
      contributorsLimit: this.state.contributorsLimit + 5
    }, ()=> {
      this.fetchData(this.state.selectedRepo);
    })
  }

  render() {
    return (
      <div className="App-Results">
        <Repositories repos={this.props.repos} onTopContributorsClick={this.onTopContributorsClick} contributorsPanelVisible={this.state.contributorsPanelVisible} />
        {
          this.state.contributorsPanelVisible &&  
          <Contributors repoName={this.state.contributorForRepoName} data={this.state.contributors} handleFetchMore={this.handleFetchMore}/>
        }
      </div>
    );
  }
}

export default Results;
