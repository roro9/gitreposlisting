import React, { Component } from 'react';

class RepoCard extends Component {
  render() {
    const {name, language, description, html_url, stargazers_count, open_issues} = this.props.data;
    return (
      <div className="repoCard">

        <div className="name"><a href={html_url} target="_blank">{name}</a></div>
        <div className="lang">{language}</div>
        <div className="desc">{description}</div>
        
        <div className="footer">
          <div className="topContrib" onClick={() => this.props.onTopContributorsClick(this.props.data)}>Top Contributors</div>
          <div className="counts">
            <div><i className="fa fa-star"></i>{stargazers_count}</div>
            <div><i className="fa fa-exclamation-triangle"></i>{open_issues}</div>
          </div>
        </div>
      </div>
    );
  }
}

class Repositories extends Component {
  render() {
    console.log('Repos');
    console.log(this.props.repos);
    const className = "repositories " + (this.props.contributorsPanelVisible ? "forceWidth" : "");
    return (
        <div className={className}>
          { !this.props.repos.length && <div>Will show results here</div> }
          
          {
            this.props.repos.map( repo => {
              return <RepoCard key={repo.id} data={repo} onTopContributorsClick={this.props.onTopContributorsClick}/>
            })
          }
        </div>
    );
  }
}

export default Repositories;
