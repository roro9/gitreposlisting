const config = {
    api: 'https://api.github.com',
    searchReposTag: 'search/repositories?q',
    limit: 6,
};

export default config;